COBI.init('token');

// Make clock appear in upper right corner
COBI.app.clockVisible.write(false);
// Also listen to standard controller events
COBI.devkit.overrideThumbControllerMapping.write(true);

// Disable Reordering in Experience
var inEditMode = (COBI.parameters.context() == COBI.context.offRideSettings || COBI.parameters.context() == COBI.context.onRideSettings);

// Allow user to zoom in and out
COBI.hub.externalInterfaceAction.subscribe(function(action) {
  // Listen to inputs and update zoom index variable
  if ((action == 'UP' || action == 'RIGHT')) {
    zoomIn();
  }
  if ((action == 'DOWN' || action == 'LEFT')) {
    zoomOut();
  }
});

// Display detailled item names if touch interaction is allowed
COBI.app.touchInteractionEnabled.subscribe(function(touchInteractionEnabled) {
  updateInterfaceVisibility(touchInteractionEnabled);
});

// Define id, name, events, formatting functions, units and default value for each item
var definitions = [
  {
    id: 'speed',
    name: 'Speed',
    subscribe: COBI.rideService.speed.subscribe,
    unsubscribe: COBI.rideService.speed.unsubscribe,
    formatter: formatSpeedDot1,
    unit: 'km/h',
    defaultValue: '0'
  },
  {
    id: 'calories',
    name: 'Calories burnt',
    subscribe: COBI.tourService.calories.subscribe,
    unsubscribe: COBI.tourService.calories.unsubscribe,
    formatter: formatInt,
    unit: 'kcal',
    defaultValue: '0'
  },
  {
    id: 'heart_rate',
    name: 'Heart rate',
    subscribe: COBI.rideService.heartRate.subscribe,
    unsubscribe: COBI.rideService.heartRate.unsubscribe,
    formatter: formatInt,
    unit: 'bpm',
    defaultValue: '-'
  },
  {
    id: 'liters',
    name: 'Liters of water',
    unit: 'liters',
    defaultValue: '0',
    drinksmart: true
  },
  {
    id: 'lasttime',
    name: 'Minutes since last sip',
    unit: 'min',
    defaultValue: '-',
    drinksmart: true
  },
  {
    id: 'smile',
    name: 'Average status',
    unit: '',
    defaultValue: '0',
    drinksmart: true
  }
];
