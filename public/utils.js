// Formatters

function formatLocation(value) {
  return value.bearing;
}

function formatInt(value) {
  return parseInt(value);
}

function formatDot1(value) {
  return parseInt(value * 10) / 10;
}

function formatSpeedInt(value) {
  return parseInt(value * 3.6);
}

function formatSpeedDot1(value) {
  var speed = formatDot1(value * 3.6).toString();
  return enforceDot1(speed);
}

function formatDistanceDot1(value) {
  var distance = formatDot1(value / 1000).toString();
  return enforceDot1(distance);
}

function formatStopwatch(value) {
  var min = parseInt(value / 60).toString();
  var sec = parseInt(value % 60).toString();
  if (min.length == 1) min = '0' + min;
  if (sec.length == 1) sec = '0' + sec;
  return min + ':' + sec;
}

function formatMins(value) {
  return formatStopwatch(value);
}

function getTimeWithoutDrink(value) {
  var aux = 'MY911'; 
  /**  $.ajax({url: 'https://enigmatic-dawn-85841.herokuapp.com/api/bottles/911', 
    success: function(data, status){
      aux = data.id;
    },
    async: false });
  */
  return aux;
}

function getTotalLiters() {
  var aux = ''; 
  $.ajax({url: 'https://enigmatic-dawn-85841.herokuapp.com/api/bottles/911', 
    success: function(data, status){
      aux = data.totalDrinkAmount / 1000;
    },
    async: false });
  return aux;
}

function getLastTimeDrink() {
  var aux = 0; 
  $.ajax({url: 'https://enigmatic-dawn-85841.herokuapp.com/api/bottles/911', 
    success: function(data, status){
      var textDate = data.drinks[data.drinks.length-1].createdDate.split(".")[0];
      aux = ((new Date().getTime() - new Date(textDate).getTime()) / 60000).toFixed(0);
      if(aux <= 0) aux = 0;
    },
    async: false });
  return '' + aux;
}

function getsmilehtml() {
  var aux;
  $.ajax({url: 'https://enigmatic-dawn-85841.herokuapp.com/api/bottles/911', 
  success: function(data, status){
    aux = data.totalDrinkAmount
  },
  async: false });
  if(aux >= 2000) return '<i class="material-icons" style="color:green;font-size:50pt">sentiment_very_satisfied</i>';
  if(aux >= 1500) return '<i class="material-icons" style="color:yellow;font-size:50pt">sentiment_satisfied</i>';
  if(aux >= 1000) return '<i class="material-icons" style="color:orange;font-size:50pt">sentiment_dissatisfied</i>';
  if(aux < 1000) return '<i class="material-icons" style="color:red;font-size:50pt">sentiment_very_dissatisfied</i>';
}

function enforceDot1(stringValue) {
  return stringValue.indexOf('.') == -1 ? stringValue + '.0' : stringValue;
}
